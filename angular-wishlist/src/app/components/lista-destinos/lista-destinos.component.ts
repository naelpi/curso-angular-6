import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinosViajesState,NuevoDestinoAction,ElegidoFavoritoAction } from '../../models/destinos-viajes-state.model';



@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  // destinos: string[];
  
  // destinos: DestinoViaje[];
  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded= new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if (d != null){
        this.updates.push('Se ha elegido a ' + d.nombre);
      }
    });
    store.select(state=> state.destinos.items).subscribe(items=>this.all=items);
    // this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
    //   if (d != null){
    //   this.updates.push('Se ha elegido a' + d.nombre);  
    //   }
    // });
    // this.destinos = ['GDL','DF','MTY'];
    // this.destinos = [];
  }

  ngOnInit() {

  }
  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  // guardar(nombre:string, url:string):Boolean
  // {
  //   this.destinos.push(new DestinoViaje(nombre,url));
  //   // console.log(new DestinoViaje(nombre,url));
  //   // console.log(nombre);
  //   // console.log(url);
  //     // console.log(this.destinos);
  //    return false;
  // }
  elegido(e: DestinoViaje)
  {
    // this.destinos.forEach(function (x) {x.setSelected(false); });
    // d.setSelected(true);
    // this.destinosApiClient.getAll().forEach(x=>x.setSelected(false));
    // e.setSelected(true);
    this.destinosApiClient.elegir(e);
  }
  getAll(){
  }
}
