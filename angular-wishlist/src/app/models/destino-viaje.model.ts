import {v4 as uuid} from 'uuid';
export class DestinoViaje
{
//    private selected:boolean
//    public servicios:string[];
    selected: boolean;
    servicios: string[];
   id = uuid();
    // nombre:string;
    // imagenUrl:string;
    
    constructor(public nombre:string, public u:string, public votes:number=0)
    {
        this.servicios = ['desayuno', 'alberca'];
    }
 
    isSelected(): boolean
    {
        return this.selected;
    }
    setSelected(d:boolean)
    {
        this.selected = d;
    }
    // {
    //     this.nombre =n;
    //     this.imagenUrl=u;
    // }
    voteUp(){
        this.votes++;
    }
    
    voteDown(){
        this.votes--;
    }
    
}

