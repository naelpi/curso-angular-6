import {reducerDestinosViajes,
    DestinosViajesState,
    intializeDestinosViajeState, 
    InitMyDataAction,
    NuevoDestinoAction} from './destinos-viajes-state.model';
    import { DestinoViaje } from './destino-viaje.model';


    describe('reducerCruceroViajes', () => { 
        it('should reducer init data', () => {
            const prevState: DestinosViajesState =  intializeDestinosViajeState();
            const action: InitMyDataAction = new InitMyDataAction(['destino 1' ,'destino 2']);
            const newState: DestinosViajesState = reducerDestinosViajes(prevState,action);
            expect(newState.items.length).toEqual(2);
            expect(newState.items[0].nombre).toEqual('destino 1');
        });

        it('should reducer new item added', () => {
            const prevState: DestinosViajesState =  intializeDestinosViajeState();
            const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('cancun','url'));
            const newState: DestinosViajesState = reducerDestinosViajes(prevState,action);
            expect(newState.items.length).toEqual(1);
            expect(newState.items[0].nombre).toEqual('cancun');
        });
    });